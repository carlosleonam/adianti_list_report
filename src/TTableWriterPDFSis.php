<?php
/**
 * PDF Writer
 *
 * @version    5.7
 * @author     Pablo Dall'Oglio
 * @copyright  Copyright (c) 2006 Adianti Solutions Ltd. (http://www.adianti.com.br)
 * @license    http://www.adianti.com.br/framework-license
 *
 * Extends by
 * Leonam, Carlos (https://github.com/carlosleonam)
 */

namespace CarlosLeonam\GeneratorReport;

class TTableWriterPDFSis extends TTableWriterPDFfork
{
    private $pdf;
    public $arg1;
    public $arg2;


    /**
     * Constructor
     * @param $widths Array with column widths
     */
    public function __construct($widths, $orientation='P', $format = 'A4')
    {
        parent::__construct($widths, $orientation, $format);
        // cria o objeto FPDF
        $this->pdf = new FPDFExtra($orientation, 'pt', $format);
        $this->pdf->Open();
        $this->pdf->AddPage();

        //Disable automatic page break
        $this->pdf->SetAutoPageBreak(false);

    }

}
