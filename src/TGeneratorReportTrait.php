<?php

namespace CarlosLeonam\GeneratorReport;

use CarlosLeonam\GeneratorReport\AdditionalFunctions;

// namespace Adianti\Registry;

/*
use Adianti\Registry\TSession;
// use Adianti\Database\Transaction;
use Adianti\Database\TCriteria;
use Adianti\Database\TRepository;
use Adianti\Widget\Dialog\TMessage;
*/

/**
 *
 */
trait TGeneratorReportTrait
{

    public function onGenerateHtml($param = null)
    {
        $result_test = $this->checkHugeQtRows();
        if ($result_test[0]) {
            if (is_string($result_test[1])) {
                AdditionalFunctions::swalert('Alerta!', $result_test[1] ,'error');
            } else {
                AdditionalFunctions::swalert('Excesso!','Mais de 1.000 registros selecionados!','error');
            }
            return false;
        }

        // Check current ORDER
        $current_order_cookie = AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_current_order');
        $current_order = [
            'order'       => $current_order_cookie[0],
            'direction'   => $current_order_cookie[1],
            'neworder'    => $current_order_cookie[2],
            'order_total' => $current_order_cookie[3],
            'group_title' => $current_order_cookie[4],
            'group_type'  => $current_order_cookie[5],
        ];

        // Array Columns Width
        $array_widths = array_slice( AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_col_width'), self::$buttons_columns);
        $array_totals = $array_widths;
        $array_totals = array_fill( 0, count( $array_totals ), false );
        foreach (self::$columns_with_total as $column) {
            $array_totals[ $column -self::$buttons_columns ] = true;
        }

        // j($array_totals);

        // Formatando o Cabeçalho
        $form_name = AdditionalFunctions::getFormName( $this->form );
        $form_name_prefix = substr_count($form_name, 'Listagem') == 0 ? 'Listagem de ' : '';

        // if ( isset($param['class_name']) && $param['class_name'] && ( $param['class_name'] != '_filters' ) ) {
        if ( isset($param['class_name']) && $param['class_name'] && ( $param['class_name'] != '_filters' ) && !isset(self::$without_header_date) ) {

            $date_print = (isset(self::$header_date) && self::$header_date)  ? '          ' . date('d/m/Y H:i') : '';
            // $total_print = ($this->total_filtered && $this->total_filtered > 0) ? number_format( $this->total_filtered, 2,',','.' ) : '';
            $total_print = AdditionalFunctions::getTotalModelByFilter([
                // 'model' => 'Ctpagar',
                'model' => $param['model_name'],
                'session_var' => $param['class_name'],
            ]);
            $form_name = AdditionalFunctions::getFormName( $this->form );
            // $report_header = "<div style=\"display: grid; grid-template-columns: 20% 60% 20%; grid-template-rows: 1fr; grid-template-areas: '. . .';\"><div>$date_print</div><div>Listagem de $form_name</div><div>$total_print</div></div>";
            $sub_title_style = "style='font-size:0.8em;text-align:right'";
            // $report_header = "<div style=\"display: grid; grid-template-columns: 60% 20% 20%; grid-template-rows: 1fr; grid-template-areas: '. . .';\"><div>Listagem de $form_name</div><div $sub_title_style'>( $date_print</div><div $sub_title_style'>R$ $total_print )</div></div>";
            $report_header = "<div style=\"display: grid; grid-template-columns: 60% 20% 20%; grid-template-rows: 1fr; grid-template-areas: '. . .';\"><div>$form_name_prefix $form_name</div><div $sub_title_style'>( $date_print</div><div $sub_title_style'>R$ $total_print )</div></div>";
        } else {
            // $report_header = 'Listagem de ' . AdditionalFunctions::getFormName( $this->form );
            $report_header = $form_name_prefix . AdditionalFunctions::getFormName( $this->form );
        }

        $report_generator = new TGeneratorReport(
            self::$database,
            self::$activeRecord,
            $this->datagrid->getColumns(),
            // 'Listagem de ' . AdditionalFunctions::getFormName( $this->form ) . ,
            // 'Listagem de ' . AdditionalFunctions::getFormName( $this->form ) . $date_print,
            $report_header,
            $this->onSearchToSession(),
            'html'
            ,
            null,
            $array_widths,
            $array_totals,
            $current_order,
            null // ,
            // true
        );
    }


    public function onGeneratePdf($param = null)
    {
        $result_test = $this->checkHugeQtRows();
        if ($result_test[0]) {
            if (is_string($result_test[1])) {
                AdditionalFunctions::swalert('Alerta!', $result_test[1] ,'error');
            } else {
                AdditionalFunctions::swalert('Excesso!','Mais de 1.000 registros selecionados!','error');
            }
            return false;
        }
        // Array Columns Width
        // $array_widths = array_slice( AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_col_width'), self::$buttons_columns); REMOVE in future
        $array_widths = AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_col_width');

        $array_totals = $array_widths;
        $array_totals = array_fill( 0, count( $array_totals ), false );
        foreach (self::$columns_with_total as $column) {
            $array_totals[ $column -self::$buttons_columns ] = true;
        }
        // Current Order
        // $current_order_cookie = AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_current_order');
        // Check current ORDER
        $current_order_cookie = AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_current_order');
        $current_order = [
            'order'       => $current_order_cookie[0],
            'direction'   => $current_order_cookie[1],
            'neworder'    => $current_order_cookie[2],
            'order_total' => $current_order_cookie[3],
            'group_title' => $current_order_cookie[4],
            'group_type'  => $current_order_cookie[5],
        ];

        // Formatando o Cabeçalho
        $form_name = AdditionalFunctions::getFormName( $this->form );
        $form_name_prefix = substr_count($form_name, 'Listagem') == 0 ? 'Listagem de ' : '';
        if ( isset($param['class_name']) && $param['class_name'] && ( $param['class_name'] != '_filters' ) ) {

            $date_print = (isset(self::$header_date) && self::$header_date)  ? '          ' . date('d/m/Y H:i') : '';
            $total_print = AdditionalFunctions::getTotalModelByFilter([
                'model' => $param['model_name'],
                'session_var' => $param['class_name'],
            ]);
            // $report_header = "Listagem de $form_name<|>$date_print<|>$total_print";
            $report_header = "$form_name_prefix $form_name<|>$date_print<|>$total_print";

        } else {
            // $report_header = 'Listagem de ' . AdditionalFunctions::getFormName( $this->form );
            $report_header = $form_name_prefix . AdditionalFunctions::getFormName( $this->form );
        }

        // j($report_header);

        /* $current_order = [
            'order'       => $current_order_cookie[0],
            'direction'   => $current_order_cookie[1],
            'neworder'    => $current_order_cookie[2],
            'order_total' => $current_order_cookie[3],
            'group_title' => $current_order_cookie[4],
            'group_type'  => $current_order_cookie[5],
        ]; */

        $columns_datagrid = $this->datagrid->getColumns();
        if (self::$columns_to_hidden) {
            foreach (array_reverse(self::$columns_to_hidden) as $column_remove) {
                // remove from datagrid columns
                unset( $columns_datagrid[$column_remove] );
                // remove respective width
                unset( $array_widths[$column_remove] );
            }
        }

        $array_totals = $array_widths;
        $array_totals = array_fill( 0, count( $array_totals ), false );
        foreach (self::$columns_with_total as $column) {
            $array_totals[ $column - self::$buttons_columns ] = true;
        }

        $report_generator = new TGeneratorReport(
            self::$database,
            self::$activeRecord,
            // $this->datagrid->getColumns(),
            $columns_datagrid,
            // 'Listagem de ' . AdditionalFunctions::getFormName( $this->form ),

            $report_header,
            $this->onSearchToSession(),
            'pdf'
            ,
            'L',
            $array_widths,
            $array_totals,
            // isset($this->current_order) ? $this->current_order : null
            $current_order,
            // self::$columns_to_hidden
            null // ,
            // true // Force break
        );
    }


    public function onGenerateRtf($param = null)
    {
        $result_test = $this->checkHugeQtRows();
        if ($result_test[0]) {
            if (is_string($result_test[1])) {
                AdditionalFunctions::swalert('Alerta!', $result_test[1] ,'error');
            } else {
                AdditionalFunctions::swalert('Excesso!','Mais de 1.000 registros selecionados!','error');
            }
            return false;
        }
        // Array Columns Width
        $array_widths = array_slice( AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_col_width'), self::$buttons_columns);
        $array_totals = $array_widths;
        $array_totals = array_fill( 0, count( $array_totals ), false );
        foreach (self::$columns_with_total as $column) {
            $array_totals[ $column -self::$buttons_columns ] = true;
        }
        $report_generator = new TGeneratorReport(
            self::$database,
            self::$activeRecord,
            $this->datagrid->getColumns(),
            'Listagem de ' . AdditionalFunctions::getFormName( $this->form ),
            $this->onSearchToSession(),
            'rtf'
            ,
            'L',
            $array_widths,
            $array_totals
        );
    }


    public function onGenerateXls($param = null)
    {
        $result_test = $this->checkHugeQtRows();
        if ($result_test[0]) {
            if (is_string($result_test[1])) {
                AdditionalFunctions::swalert('Alerta!', $result_test[1] ,'error');
            } else {
                AdditionalFunctions::swalert('Excesso!','Mais de 1.000 registros selecionados!','error');
            }
            return false;
        }

        // Check current ORDER
        $current_order_cookie = AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_current_order');
        $current_order = [
            'order'       => $current_order_cookie[0],
            'direction'   => $current_order_cookie[1],
            'neworder'    => $current_order_cookie[2],
            'order_total' => $current_order_cookie[3],
            'group_title' => $current_order_cookie[4],
            'group_type'  => $current_order_cookie[5],
        ];

        // Array Columns Width
        $array_widths = array_slice( AdditionalFunctions::checkCookieForTDatagrid('profile_tdatagrid_'. self::$formName .'_col_width'), self::$buttons_columns);
        $array_totals = $array_widths;
        $array_totals = array_fill( 0, count( $array_totals ), false );
        foreach (self::$columns_with_total as $column) {
            $array_totals[ $column -self::$buttons_columns ] = true;
        }

        // Formatando o Cabeçalho
        if ( isset($param['class_name']) && $param['class_name'] && ( $param['class_name'] != '_filters' ) ) {

            $date_print = (isset(self::$header_date) && self::$header_date)  ? '          ' . date('d/m/Y H:i') : '';
            $total_print = AdditionalFunctions::getTotalModelByFilter([
                'model' => $param['model_name'],
                'session_var' => $param['class_name'],
            ]);
            $form_name = AdditionalFunctions::getFormName( $this->form );
            $report_header = "Listagem de $form_name<|>$date_print<|>$total_print";
            // $report_header = "Listagem de $form_name<|>$date_print<|>$total_print";

        } else {
            $report_header = 'Listagem de ' . AdditionalFunctions::getFormName( $this->form );
        }

        $report_generator = new TGeneratorReport(
            self::$database,
            self::$activeRecord,
            $this->datagrid->getColumns(),
            // 'Listagem de ' . AdditionalFunctions::getFormName( $this->form ),
            $report_header,
            $this->onSearchToSession(),
            'xls'
            ,
            $this->reports_orientation ?? 'L',
            $array_widths,
            $array_totals,
            $current_order,
            null // ,
            // true
        );
    }


    /**
     * Check if has a huge qtty of rows to report
     *
     * @return void
     */
    public function checkHugeQtRows()
    {
        $filters = $this->onSearchToSession();

        if (!$filters) {
            $result = [];
            $result[] = true;
            $result[] = 'Não foi efetuada nenhuma Filtragem. Efetue uma busca antes de tentar imprimir!';
            return $result;
        }

        try
        {
            \Adianti\Database\TTransaction::open(self::$database); // open a transaction

        // creates a repository for Clientes
            $repository = new \Adianti\Database\TRepository(self::$activeRecord);

            // creates a criteria
            $criteria = new \Adianti\Database\TCriteria;

            if($filters = \Adianti\Registry\TSession::getValue(__CLASS__.'_filters'))
            {
                foreach ($filters as $filter)
                {
                    // $criteria->add($filter);
                    $filter_sql = $filter->dump();
                    if (substr_count($filter_sql,'||') > 0 ) {

                        $filter_sql = str_replace(['%',"'"],['',''], $filter_sql);
                        $filter_field = explode(' ', $filter_sql)[0];
                        $filter_operator = explode(' ', $filter_sql)[1];
                        $filters_or = explode('||', explode(' ', $filter_sql)[2]);

                        $operator_complement = $filter_operator == 'like' ? '%' : '';

                        $criteria_or = new \Adianti\Database\TCriteria;
                        foreach ($filters_or as $filter_value) {

                            // $filter_sub = new TFilter( $filter_field, $filter_operator, "NOESC:'%$filter_value%'" ) ;  // operator =, <, >, BETWEEN, IN, NOT IN, LIKE, IS NOT
                            $filter_sub = new TFilter( $filter_field, $filter_operator, "NOESC:'$operator_complement$filter_value$operator_complement'" ) ;  // operator =, <, >, BETWEEN, IN, NOT IN, LIKE, IS NOT

                            $criteria_sub = new \Adianti\Database\TCriteria;
                            $criteria_sub->add($filter_sub);
                            $criteria_or->add($criteria_sub, TExpression::OR_OPERATOR);
                        }
                        $criteria->add($criteria_or);

                    } else {

                    $criteria->add($filter);
                }

                }
            }

            // load the objects according to criteria
            $objects_count = $repository->count($criteria); //, FALSE);

            \Adianti\Database\TTransaction::close(); // close the transaction
        }
        catch (Exception $e) // in case of exception
        {
            new \Adianti\Widget\Dialog\TMessage('error', $e->getMessage()); // shows the exception error message
            \Adianti\Database\TTransaction::rollback(); // undo all pending operations
        }

        if ($objects_count > (self::$limit_records ?? 1000)) {
            // return true;
            $result = [];
            $result[] = true;
            $result[] = null;
            return $result;
        } else {
            // return false;
            $result = [];
            $result[] = false;
            return $result;
        }

    }

    /**
     * Send onSearch params to Session
     *
     * @return void
     */
    public function onSearchToSession()
    {
        // return TSession::getValue(__CLASS__.'_filters');
        // return TSession::getValue( get_class($this) . '_filters');
        return \Adianti\Registry\TSession::getValue( get_class($this) . '_filters');
    }


}
