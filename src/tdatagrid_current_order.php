<?php
/*
 * Created Date: Tuesday, October 22nd 2019, 7:48:20 am
 * Author: Leonam, Carlos
 *
 * Copyright (c) 2019 SisSoftwaresWEB
 */
// var current_order = escape(". implode(',', $this->current_order) .");

/* TScript::create("
var current_order = escape('". implode(',', [$param['order'], $param['direction']]) ."');
document.cookie=\"profile_tdatagrid_". self::$formName ."_current_order = \" + current_order;
"); */

/* *
    * Definição do COOKIE que conterá:
    *   - ORDEM
    *   - DIRECAO
    *   - DIRECAO_FAKE
    *   - COLUNA A TOTALIZAR
    *   - TEXTO A RENDERIZAR NA QUEBRA
    */
TScript::create("
    var current_order = escape('". implode(',',
        [
            $param['order'],
            $param['direction'],
            $newparam['order'] ?? '',
            // 'CTP_VALOR',
            // 'Vencimento: {CTP_VENCTO_transformed}',
            $this->order_total,
            $this->break_title,
            $this->group_type ?? '',
        ]
    ) ."');
    document.cookie=\"profile_tdatagrid_". self::$formName ."_current_order = \" + current_order;
");

