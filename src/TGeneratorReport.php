<?php

namespace CarlosLeonam\GeneratorReport;
/*
use Adianti\Registry\TSession;
use Adianti\Database\Transaction;
use Adianti\Database\TCriteria;
use Adianti\Database\TRepository;
use Adianti\Widget\Dialog\TMessage;
*/


/**
 * Report Generator from Adianti TDatagrid
 *
 * @author Leonam, Carlos (https://github.com/carlosleonam)
 * @license MIT
 */
class TGeneratorReport
{

    public $database;
    public $active_record;
    public $datagrid_columns;
    public $form_title;
    public $filters;
    public $type_relat;
    public $widths;
    public $position;
    public $totals;
    public $order;
    public $columns_to_hidden;
    public $has_break;
    public $report_title;
    public $group_type;

    /**
     * __construct
     *
     * @param string $database
     * @param string $active_record
     * @param object $datagrid_columns
     * @param string $form_title
     * @param TCriteria $filters
     * @param string $type_relat
     * @param string $position
     * @param array $widths
     * @param array $totals
     * @param string $order
     */
    function __construct( $database, $active_record, $datagrid_columns, $form_title, $filters, $type_relat = 'pdf', $position = 'L',
                          $widths = null, $totals = null, $order = null, $columns_to_hidden = null, $has_break = false )
    {
        $this->database = $database;
        $this->active_record = $active_record;
        $this->datagrid_columns = $datagrid_columns;
        $this->form_title = $form_title;
        $this->filters = $filters;
        $this->type_relat = $type_relat;
        $this->position = $position;
        $this->widths = $widths;
        // $this->widths = $columns_to_hidden ? array_slice( $widths, (count($columns_to_hidden)-1) )  : $widths;
        $this->totals = $totals;
        $this->order = $order;
        // $this->columns_to_hidden = $columns_to_hidden ?? [];
        $this->has_break = $has_break;

        $this->onGenerate( $type_relat );

    }


    public function onGenerate($format)
    {
        try
        {
            $filters = $this->filters;
            // open a transaction with database 'small_erp'
            \Adianti\Database\TTransaction::open($this->database);
            $param = [];

            // Set report ORDER
            if ($this->order) {
                $param['order'] = $this->order['order'];
                if (isset($this->order['direction'])) {
                    $param['direction'] = $this->order['direction'];
                }
                if (isset($this->order['neworder'])) {
                    $param['neworder'] = $this->order['neworder'];
            }
                if (isset($this->order['order_total'])) {
                    $param['order_total'] = $this->order['order_total'];
            }
                if (isset($this->order['group_type'])) {
                    $param['group_type'] = $this->order['group_type'];
                }
            }
            // creates a repository for Active Record
            $repository = new \Adianti\Database\TRepository( $this->active_record );
            // creates a criteria
            $criteria = new \Adianti\Database\TCriteria;

            $criteria->setProperties($param);

            $filtragem = null;
            if ($filters)
            {
                foreach ($filters as $filter)
                {
                    // $criteria->add($filter);

                    $filter_sql = $filter->dump();
                    if (substr_count($filter_sql,'||') > 0 ) {

                        $filter_sql = str_replace(['%',"'"],['',''], $filter_sql);
                        $filter_field = explode(' ', $filter_sql)[0];
                        $filter_operator = explode(' ', $filter_sql)[1];
                        $filters_or = explode('||', explode(' ', $filter_sql)[2]);

                        $operator_complement = $filter_operator == 'like' ? '%' : '';

                        $criteria_or = new \Adianti\Database\TCriteria;
                        foreach ($filters_or as $filter_value) {

                            // $filter_sub = new TFilter( $filter_field, $filter_operator, "NOESC:'%$filter_value%'" ) ;  // operator =, <, >, BETWEEN, IN, NOT IN, LIKE, IS NOT
                            $filter_sub = new TFilter( $filter_field, $filter_operator, "NOESC:'$operator_complement$filter_value$operator_complement'" ) ;  // operator =, <, >, BETWEEN, IN, NOT IN, LIKE, IS NOT

                            $criteria_sub = new \Adianti\Database\TCriteria;
                            $criteria_sub->add($filter_sub);
                            $criteria_or->add($criteria_sub, TExpression::OR_OPERATOR);
                        }
                        $criteria->add($criteria_or);

                    } else {

                    $criteria->add($filter);
                }
                }
                $filtragem = $criteria->dump();
            }

            // load the objects according to criteria
            $objects = $repository->load($criteria, FALSE);

            $this->objects_qtty = count($objects);

            if ($objects)
            {
                $colunas_datagrid = $this->datagrid_columns;

                if($this->widths){
                    $widths = $this->widths;
                } else {
                    $widths = array_fill(0,count($colunas_datagrid),200); // Largura das colunas no relatório :: Pegar da Listagem
                }

                if($this->totals){
                    $totals = $this->totals;
                } else {
                    $totals = array_fill(0,count($colunas_datagrid),false); // Largura das colunas no relatório :: Pegar da Listagem
                }

                $totals_sum = array_fill(0,count($colunas_datagrid),0);

                switch ($format)
                {
                    case 'html':
                        // $tr = new TTableWriterHTMLfork($widths);
                        $tr = new TTableWriterHTMLSis($widths);
                        break;
                    case 'xls':
                        $tr = new TTableWriterXLSSis($widths);
                        break;
                    case 'pdf':
                        // $tr = new TTableWriterPDFSis($widths, 'L');
                        $tr = new TTableWriterPDFSis($widths, $this->position);
                        break;
                    case 'rtf':
                        if (!class_exists('PHPRtfLite_Autoloader'))
                        {
                            PHPRtfLite::registerAutoloader();
                        }
                        $tr = new TTableWriterRTFSis($widths, $this->position);
                        break;
                }

                if (!empty($tr))
                {
                    $font_default = '9';

                    // create the document styles
                    $tr->addStyle('title', 'Helvetica', '10', 'B',   '#000000', '#dbdbdb');
                    $tr->addStyle('filter', 'Arial', '6', '',    '#808080', '#f0f0f0');
                    $tr->addStyle('datap', 'Arial', $font_default, '',    '#333333', '#f0f0f0');
                    $tr->addStyle('datai', 'Arial', $font_default, '',    '#333333', '#ffffff');
                    $tr->addStyle('header', 'Helvetica', '16', 'B',   '#5a5a5a', '#ffffff');
                    $tr->addStyle('footer', 'Helvetica', $font_default, 'B',  '#5a5a5a', '#ffffff');
                    $tr->addStyle('break', 'Helvetica', $font_default, 'B',  '#ffffff', '#9a9a9a');
                    $tr->addStyle('total', 'Helvetica', $font_default, 'I',  '#000000', '#c7c7c7');
                    $tr->addStyle('total_final', 'Helvetica', $font_default, 'BI',  '#000000', '#c7c7c7');
                    $tr->addStyle('breakTotal', 'Helvetica', $font_default, 'I',  '#000000', '#c6c8d0');

                    // j('original',explode(' ',$filtragem));

                    $table_name = constant($this->active_record.'::TABLENAME');

                    if (class_exists('GeneralFunctions') && method_exists('GeneralFunctions','parseSQLFields') ) {

                        $filtragem = AdditionalFunctions::parseSQLFields($table_name, $filtragem);

                    }

                    // $report_title = $title_form_current;
                    $this->report_title = $this->form_title;
                    $this->report_title .= isset($filtragem) ? '<|>(' . $filtragem . ')': '';

                    // j('traduzido',explode(' ',$filtragem));

                    // Quando o relatório a ser gerado for PDF
                    if ($format == 'pdf')
                    {
                        // Adiciona o cabeçalho
                        $tr->setHeaderCallback(
                            function($tr)
                            {
                                $pdf = $tr->getNativeWriter();

                                // Define a fonte/ estilos
                                $pdf->SetFont('Arial','B',15);

                                // Define o posicionamento do texto
                                $pdf->Cell(80);

                                if (strpos( $this->report_title, '<|>' ) !== false ) {
                                    $parts = explode( '<|>', $this->report_title );
                                    $title = $parts[0];
                                    $date   = $parts[1];
                                    $total  = $parts[2];
                                    $filter = $parts[3];

                                } else {
                                    $title = $this->report_title;
                                    $filter = null;
                                }

                                // Texto do cabeçalho
                                $pdf->Cell(0,10, utf8_decode( $title ) ,0,0,'C');

                                if ($filter) {
                                    $pdf->Ln(20);
                                    $pdf->SetFont('Arial','I',6);
                                    $pdf->Cell(0,10, 'filtragem: ' . utf8_decode( $filter ) ,0,0,'R');
                                }

                                // Line break
                                $pdf->Ln(20);

                            }
                            // , $report_title, $widths
                        );

                        // Adiciona o footer do relatório
                        $tr->setFooterCallback(
                            function($tr)
                            {
                                $pdf = $tr->getNativeWriter();

                                // Necessário para obter o número total de páginas
                                $pdf->AliasNbPages();

                                // Posiciona o footer no final da página
                                $pdf->SetY(-40);

                                // Define o estilho do footer
                                $pdf->SetFont('Arial'   ,'B',12);
                                $pdf->Cell(110);

                                // Obtém o número da página atual
                                $numero = $pdf->PageNo();

                                // Footer
                                $pdf->Cell(0,10, utf8_decode("Página: {$numero}/{nb}") ,0,0,'R');

                                // Line break
                                $pdf->Ln(20);
                            }
                        );
                    }
                    else // Para os demais formatos
                    {
                         $tr->setHeaderCallback(
                            function($tr)
                            {
                                if (strpos( $this->report_title, '<|>' ) !== false ) {
                                    $parts = explode( '<|>', $this->report_title );
                                    /*
                                    $title = $parts[0];
                                    $filter = $parts[1];
                                    // $date = $parts[0];
                                    // $title = $parts[1];
                                    // $filter = $parts[2];
                                    */
                                    $title  = $parts[0];
                                    $date   = $parts[1];
                                    $total  = $parts[2] ?? '';
                                    $filter = $parts[3] ?? '';

                                } else {
                                    $title = $this->report_title;
                                    $filter = null;
                                }
                                $tr->addRow();
                                // $tr->addCell($title, 'center', 'header', count($tr->arg2));
                                $tr->addCell($title, 'center', 'header', count($this->widths));
                                if ($filter) {
                                    $tr->addRow();
                                    // $tr->addCell($filter, 'right', 'filter', count($tr->arg2));
                                    $tr->addCell($filter, 'right', 'filter', count($this->widths));
                                }

                            }
                        );

                        $tr->setFooterCallback(
                            function($tr)
                            {
                                $tr->addRow();

                                $tr->addCell( 'Qtde: '. AdditionalFunctions::get_number_wo_decimals( $this->objects_qtty ), 'right', 'breakTotal');

                                // $tr->addCell(date('Y-m-d h:i:s'), 'right', 'footer', count($tr->arg2));
                                // $tr->addCell(date('Y-m-d h:i:s'), 'right', 'footer', count($this->widths));
                                $tr->addCell(date('Y-m-d h:i:s'), 'right', 'footer', count($this->widths)-1);
                            }
                        );
                    }


                    // add titles row
                    $tr->addRow();

                    foreach ($colunas_datagrid as $key => $coluna) {
                        // Pegando o nome da coluna
                        $reflectionProperty = new \ReflectionProperty(\Adianti\Widget\Datagrid\TDataGridColumn::class, 'label');
                        $reflectionProperty->setAccessible(true);
                        // Once the property is made accessible, you can read it..
                        $label_column = $reflectionProperty->getValue($coluna);

                        if ($format == 'pdf' || $format == 'rtf') {
                            $label_column = filter_var( $label_column, FILTER_SANITIZE_STRING );
                        }

                        $tr->addCell($label_column, 'left', 'title');
                    }

                    $grandTotal = [];
                    $breakTotal = [];
                    $breakValue = null;
                    $firstRow = true;

                    // controls the background filling
                    $colour = false;

                    $group_type = '';
                    if ((null !== $this->order) && is_array($this->order)) {
                        $current_order = $this->order['order'];
                        $order_total = $this->order['order_total'];
                        $group_title = $this->order['group_title'];
                        $group_type = $this->order['group_type'];
                    }

                    if (!$this->has_break) {
                        // if(!empty($group_type) && $group_type !== 'nogroup'){
                        if($group_type !== 'nogroup' && !empty($group_type)){
                            $this->has_break = true;
                        }
                    }

                    // j($objects);

                    // Iterate with objects returned by TRepository
                    foreach ($objects as $object)
                    {
                        $style = $colour ? 'datap' : 'datai';

                        // j($object);

                        // condition to use BREAK GROUP

                        if ($this->has_break) {

                            if ((null !== $this->order) && is_array($this->order)) {

                                // Add break if $object->$current_order doesn't equals to $breakValue
                                if ($object->$current_order !== $breakValue)
                                {
                                    if (!$firstRow)
                                    {
                                        $tr->addRow();

                                        for ($qt_cols=0; $qt_cols < count($widths)-1 ; $qt_cols++) {
                                            $tr->addCell('', 'center', 'breakTotal');
                                        }

                                        $breakTotal_SUM = array_sum($breakTotal);

                                        $tr->addCell( AdditionalFunctions::numeroBR( $breakTotal_SUM ), 'right', 'breakTotal');
                                    }

                                    $tr->addRow();

                                    $tr->addCell($object->render($group_title), 'left', 'break', 30);

                                    $breakTotal = [];
                                }

                                $breakTotal[] = $object->$order_total;

                                $breakValue = $object->$current_order;
                            }

                        }

                        $firstRow = false;

                        $tr->addRow();

                        foreach ($colunas_datagrid as $key => $coluna) {
                            // Pegando o nome da coluna
                            $reflectionProperty = new \ReflectionProperty(\Adianti\Widget\Datagrid\TDataGridColumn::class, 'name');
                            $reflectionProperty->setAccessible(true);
                            // Once the property is made accessible, you can read it..
                            $name_column = $reflectionProperty->getValue($coluna);

                            // Pegando o alinhamento da coluna
                            $reflectionProperty = new \ReflectionProperty(\Adianti\Widget\Datagrid\TDataGridColumn::class, 'align');
                            $reflectionProperty->setAccessible(true);
                            // Once the property is made accessible, you can read it..
                            $align_column = $reflectionProperty->getValue($coluna);

                            $transformer = $coluna->getTransformer();

                            if (substr_count($name_column,'{') > 0) {

                                // $column_calculated_1 = $object->evaluate('=( {cliente->nome} ({cliente->id}) )');
                                // $content = $object->evaluate('=( '. strip_tags( $name_column ) .' )');
                                $content = $object->render( $name_column );
                            } else {
                            $content = $object->$name_column;
                            }

                            // Check if anable SUM this column
                            if ( isset($totals) && isset($totals[ $key ]) && $totals[ $key ] ) {
                                if ($transformer) {
                                    $value_process = call_user_func($transformer, $content, $object, null);
                                    $totals_sum[ $key ] += AdditionalFunctions::getNumberFromText( $value_process, true );
                                } else {
                                    $totals_sum[ $key ] += is_numeric( $content) ? $content : 0;
                                }
                            }

                            if ($transformer)
                            {
                                // apply the transformer functions over the data
                                $content = call_user_func($transformer, $content, $object, null);
                            }

                            if ($format == 'pdf' || $format == 'rtf') {
                                $content = filter_var( $content, FILTER_SANITIZE_STRING );
                            }

                            $tr->addCell($content, $align_column, $style);
                        }

                        $colour = !$colour;
                    }

                    if ($totals) {

                        if ($this->has_break) {
                            $tr->addRow();

                            // $tr->addCell('', 'center', 'breakTotal');
                            for ($qt_cols=0; $qt_cols < count($widths)-1 ; $qt_cols++) {
                            // for ($qt_cols=0; $qt_cols < count($widths)-1 - count($this->columns_to_hidden) ; $qt_cols++) {
                                $tr->addCell('', 'center', 'breakTotal');
                                // $tr->addCell('X', 'center', 'breakTotal');
                            }
                            $breakTotal_SUM = array_sum($breakTotal);
                            $tr->addCell( AdditionalFunctions::numeroBR( $breakTotal_SUM ), 'right', 'breakTotal');
                        }

                        $tr->addRow();

                        foreach ($colunas_datagrid as $key => $coluna) {

                            // if ($totals[ $key ]) {
                            if ( isset($totals) && isset($totals[ $key ])) {
                                $tr->addCell( AdditionalFunctions::numberBR( $totals_sum[ $key ] ) , 'right', 'total_final');
                            } else {
                                $tr->addCell('', 'center', 'total');
                            }
                        }
                    } else {
                        $tr->addRow();
                        $tr->addCell( 'Qtde: '. AdditionalFunctions::get_number_wo_decimals( count($objects)), 'right', 'breakTotal');

                    }



                    $file = 'report_'.uniqid().".{$format}";
                    // stores the file
                    if (!file_exists("app/output/{$file}") || is_writable("app/output/{$file}"))
                    {
                        $tr->save("app/output/{$file}");
                    }
                    else
                    {
                        throw new Exception(_t('Permission denied') . ': ' . "app/output/{$file}");
                    }

                    \Adianti\Control\TPage::openFile("app/output/{$file}");

                    // shows the success message
                    $output = "app/output/{$file}";
                    new \Adianti\Widget\Dialog\TMessage('info', _t('Report generated. Please, enable popups') . ". <br> <a href='$output'>clique aqui para download</a>");
                }
            }
            else
            {
                new \Adianti\Widget\Dialog\TMessage('error', _t('No records found'));
            }

            // close the transaction
            \Adianti\Database\TTransaction::close();
        }
        catch (Exception $e) // in case of exception
        {
            // shows the exception error message
            new \Adianti\Widget\Dialog\TMessage('error', $e->getMessage());
            // undo all pending operations
            \Adianti\Database\TTransaction::rollback();
        }
    }

}

