<?php

        /**
         * Botôes para Impressão da TDataGrid
         */

        // Defined Class Name Called
        $class_name = ( isset(self::$class_name) && ( self::$class_name != '' ) ) ? self::$class_name : '';

        TPage::register_css('dropdown-menu','
             /** Dropdowns **/
             .dropdown-menu li a {
                 display: block;
                 text-decoration: unset;
                 padding: 8px 12px;
                 white-space: nowrap;
             }
        ');

        $textMsg = 'Este é um Gerador de Relatórios a partir do<br>
                    resultado da consulta efetuada.<br><br>
                    Escolha o formato desejado.<br>
                    Lembre de efetuar a consulta antes!';

        if (class_exists('TTutorialAccess')) {

            $tutorial_access = new TTutorialAccess('TGeneratorReport',null,null,null, 'right', 'RELATÓRIOS RÁPIDOS', $textMsg, false );
        } else {
            $tutorial_access = '';
        }

        // header actions
        // $dropdown = new TDropDown('Relatórios Rápidos', 'fa:list');
        // $dropdown = new TDropDown('Relatórios Rápidos' . $span, 'fa:list');
        $dropdown = new TDropDown('Relatórios Rápidos' . $tutorial_access, 'fa:list');
        $dropdown->setPullSide('right');
        $dropdown->setButtonClass('btn btn-default waves-effect dropdown-toggle');
        $dropdown->addAction( 'HTML', new TAction([$this, 'onGenerateHtml'], ['register_state' => 'false', 'static'=>'1', 'class_name'=> $class_name.'_filters', 'model_name'=> self::$activeRecord ]), 'fa:table #69aa46' );
        $dropdown->addAction( 'PDF', new TAction([$this, 'onGeneratePdf'], ['register_state' => 'false', 'static'=>'1', 'class_name'=> $class_name.'_filters', 'model_name'=> self::$activeRecord ]), 'fa:file-pdf #d44734' );
        // $dropdown->addAction( 'RTF (doc)', new TAction([$this, 'onGenerateRtf'], ['register_state' => 'false', 'static'=>'1', 'class_name'=> $class_name.'_filters', 'model_name'=> self::$activeRecord ]), 'fa:file-text-o #324bcc' );
        $dropdown->addAction( 'XLS', new TAction([$this, 'onGenerateXls'], ['register_state' => 'false', 'static'=>'1', 'class_name'=> $class_name.'_filters', 'model_name'=> self::$activeRecord ]), 'fa:file-excel #cc7932' );
        $panel->addHeaderWidget( $dropdown );
