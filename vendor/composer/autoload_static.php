<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitca5a12f7d1f1232f3462683e2a7fec24
{
    public static $files = array (
        'cf65e956ef36045d86a2d654adf24874' => __DIR__ . '/..' . '/varunsridharan/sweetalert2-php/sweetalert2.php',
    );

    public static $prefixLengthsPsr4 = array (
        'C' => 
        array (
            'CarlosLeonam\\GeneratorReport\\' => 29,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'CarlosLeonam\\GeneratorReport\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitca5a12f7d1f1232f3462683e2a7fec24::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitca5a12f7d1f1232f3462683e2a7fec24::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitca5a12f7d1f1232f3462683e2a7fec24::$classMap;

        }, null, ClassLoader::class);
    }
}
